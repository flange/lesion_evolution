#!/bin/bash

proj_dir="/home/flange/Sandbox/OHBM_2024"

# Create brain and lesion masks
sub_list=(sub_a sub_b sub_c)
for sub in ${sub_list[@]}
do
    t_list=($(ls -d ${proj_dir}/${sub}/t_?))
    for t in ${t_list[@]}
    do
        sub_dir="${t}/anat"
        fslmaths ${sub_dir}/T1_biascorr_brain_synth_mask -s 0.5 ${sub_dir}/T1_unmask
        fslmaths ${sub_dir}/T2Lesion_in_T1 -binv -ero -mul ${sub_dir}/T1_biascorr_brain_synth_mask -s 0.5 ${sub_dir}/T1_mask
    done
done