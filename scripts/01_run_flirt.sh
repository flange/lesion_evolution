#!/bin/bash

proj_dir="/home/flange/Sandbox/OHBM_2024"

sub_list=(sub_a sub_b sub_c)
for sub in ${sub_list[@]}
do
    # Register timepoint 0 to template
    flirt \
        -in ${proj_dir}/${sub}/t_0/anat/T1_biascorr_brain_synth \
        -ref ${proj_dir}/template/OMM-1_T1_brain \
        -dof 6 \
        -out ${proj_dir}/${sub}/t_0/anat/T1_to_OMM_lin \
        -omat ${proj_dir}/${sub}/t_0/anat/T1_to_OMM.mat \
        -v

    # Register remaining timepoints to timepoint 0 and concatenate to template
    t_list=($(ls -d ${proj_dir}/${sub}/t_?))
    for t in ${t_list[@]:1}
    do
        flirt \
            -in ${t}/anat/T1_biascorr_brain_synth \
            -ref ${proj_dir}/${sub}/t_0/anat/T1_biascorr_brain_synth \
            -dof 6 \
            -out ${t}/anat/T1_to_t_0_lin \
            -omat ${t}/anat/T1_to_t_0.mat \
            -v

        convert_xfm \
            -omat ${t}/anat/T1_to_OMM.mat \
            -concat ${proj_dir}/${sub}/t_0/anat/T1_to_OMM.mat ${t}/anat/T1_to_t_0.mat
    done
done