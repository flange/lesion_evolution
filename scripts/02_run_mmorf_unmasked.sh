#!/bin/bash

proj_dir="/home/flange/Sandbox/OHBM_2024"

# Register remaining timepoints to timepoint 0
sub_list=(sub_a sub_b sub_c)
for sub in ${sub_list[@]}
do
    t_list=($(ls -d ${proj_dir}/${sub}/t_?))
    for t in ${t_list[@]:1}
    do
        ref_dir="${proj_dir}/${sub}/t_0/anat"
        mov_dir="${t}/anat"
        mmorf \
            --version \
            --config ${proj_dir}/config/T1_masked.ini \
            --img_warp_space ${proj_dir}/template/OMM-1_T1_brain \
            --img_ref_scalar ${ref_dir}/T1_biascorr_brain_synth \
            --img_mov_scalar ${mov_dir}/T1_biascorr_brain_synth \
            --aff_ref_scalar ${ref_dir}/T1_to_OMM.mat \
            --aff_mov_scalar ${mov_dir}/T1_to_OMM.mat \
            --mask_ref_scalar ${ref_dir}/T1_unmask \
            --mask_mov_scalar ${mov_dir}/T1_unmask \
            --warp_out ${mov_dir}/warp_T1_unmasked \
            --jac_det_out ${mov_dir}/jac_T1_unmasked \
            --bias_out ${mov_dir}/bias_T1_unmasked > ${mov_dir}/mmorf_unmasked.log
    done
done