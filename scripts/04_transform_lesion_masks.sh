#!/bin/bash

proj_dir="/home/flange/Sandbox/OHBM_2024"

sub_list=(sub_a sub_b sub_c)
for sub in ${sub_list[@]}
do
    t_list=($(ls -d ${proj_dir}/${sub}/t_?))
    for t in ${t_list[@]}
    do
        sub_dir="${t}/anat"
        # Rigid transform all timepoints to OMM
        applywarp \
            --in=${sub_dir}/T2Lesion_in_T1 \
            --out=${sub_dir}/T2Lesion_in_OMM_lin \
            --ref=${proj_dir}/template/OMM-1_T1_brain \
            --premat=${sub_dir}/T1_to_OMM.mat \
            --interp=trilinear \
            --verbose
    done

    # Simple copy of timepoint 0 rigid to nonlinear
    imcp \
        ${proj_dir}/${sub}/t_0/anat/T2Lesion_in_OMM_lin \
        ${proj_dir}/${sub}/t_0/anat/T2Lesion_in_OMM_nln


    for t in ${t_list[@]:1}
    do
        sub_dir="${t}/anat"
        # Nonlinear transform all other timepoints to OMM using masked warp
        applywarp \
            --in=${sub_dir}/T2Lesion_in_T1 \
            --out=${sub_dir}/T2Lesion_in_OMM_nln \
            --ref=${proj_dir}/template/OMM-1_T1_brain \
            --premat=${sub_dir}/T1_to_OMM.mat \
            --warp=${sub_dir}/warp_T1_masked \
            --interp=trilinear \
            --verbose
    done
done