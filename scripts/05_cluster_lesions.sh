#!/bin/bash

proj_dir="/home/flange/Sandbox/OHBM_2024"

sub_list=(sub_a sub_b sub_c)
for sub in ${sub_list[@]}
do
    sub_dir="${proj_dir}/${sub}"
    # Combine linear and nonlinear lesion maps
    fslmerge \
        -t \
        ${sub_dir}/tracking/lesions_merged \
        $(ls ${sub_dir}/t_?/anat/T2Lesion_in_OMM*)

    # Collapse across time
    fslmaths \
        ${sub_dir}/tracking/lesions_merged \
        -Tmax \
        -thr 0.0 \
        -bin \
        ${sub_dir}/tracking/lesions_collapsed

    # Cluster lesions
    cluster \
        -i ${sub_dir}/tracking/lesions_collapsed \
        -t 0.1 \
        -o ${sub_dir}/tracking/cluster_indices \
        > ${sub_dir}/tracking/cluster_indices.txt
done