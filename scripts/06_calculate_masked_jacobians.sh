#!/bin/bash

proj_dir="/home/flange/Sandbox/OHBM_2024"

sub_list=(sub_a sub_b sub_c)
for sub in ${sub_list[@]}
do
    sub_dir="${proj_dir}/${sub}"
    # Calculate for timepoint 0 (assume |J| = 1)
    imcp ${sub_dir}/t_0/anat/T2Lesion_in_OMM_lin ${sub_dir}/t_0/anat/jac_weighted
    imcp ${sub_dir}/t_0/anat/T2Lesion_in_OMM_lin ${sub_dir}/t_0/anat/jac_weighted_masked

    # Calculate for remaining timepoints
    t_list=($(ls -d ${sub_dir}/t_?))
    for t in ${t_list[@]:1}
    do
        fslmaths ${t}/anat/jac_T1_unmasked -mul ${sub_dir}/t_0/anat/T2Lesion_in_OMM_lin ${t}/anat/jac_weighted
        fslmaths ${t}/anat/jac_T1_masked -mul ${sub_dir}/t_0/anat/T2Lesion_in_OMM_lin ${t}/anat/jac_weighted_masked
    done
done