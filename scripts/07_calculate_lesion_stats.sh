#!/bin/bash

proj_dir="/home/flange/Sandbox/OHBM_2024"

sub_list=(sub_a sub_b sub_c)
for sub in ${sub_list[@]}
do
    sub_dir="${proj_dir}/${sub}"
    n_clusters="$(awk 'NR == 2 {print $1}' ${sub_dir}/tracking/cluster_indices.txt)"

    t_list=($(ls -d ${sub_dir}/t_?))
    for t in ${t_list[@]}
    do
        # Calculate linear cluster stats
        echo "vox vol mean" > ${t}/anat/lesion_stats_lin
        fslstats -K ${sub_dir}/tracking/cluster_indices ${t}/anat/T2Lesion_in_OMM_lin -V -M >> ${t}/anat/lesion_stats_lin

        # Calculate nonlinear cluster stats
        echo "vox vol mean" > ${t}/anat/lesion_stats_nln
        fslstats -K ${sub_dir}/tracking/cluster_indices ${t}/anat/T2Lesion_in_OMM_nln -V -M >> ${t}/anat/lesion_stats_nln

        # Calculate jacobian cluster stats 
        echo "vox vol mean" > ${t}/anat/lesion_stats_jac
        fslstats -K ${sub_dir}/tracking/cluster_indices ${t}/anat/jac_weighted -V -M >> ${t}/anat/lesion_stats_jac

        # Calculate jacobian cluster stats 
        echo "vox vol mean" > ${t}/anat/lesion_stats_jac_masked
        fslstats -K ${sub_dir}/tracking/cluster_indices ${t}/anat/jac_weighted_masked -V -M >> ${t}/anat/lesion_stats_jac_masked
    done
done